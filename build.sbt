name := "test"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache
)

// scala 2.11 の場合は　2.7.3 らしい
// 2.1.0 2.6.3
libraryDependencies += "org.mongodb" %% "casbah" % "2.6.3"

play.Project.playScalaSettings
