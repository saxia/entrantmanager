package controllers

import java.text.{SimpleDateFormat, DateFormat}
import java.util.TimeZone
import com.google.common.util.concurrent.ListenableFutureTask
import domain.Admin.EntrantAdministrator
import domain.{MeetingErrorState, Meeting, EntrantUser}
import domain.Repository.{optionData, MeetingRepository, EventRepository}
import play.api._
import play.api.libs.concurrent.Redeemed
import play.api.mvc._
import play.api.libs.openid._
import play.api.libs.concurrent.Execution.Implicits._
import play.mvc.Http
import scala.concurrent._
import java.net._
import scala.collection._
import scala.collection.JavaConversions._


trait Auth {
  def mail(request: RequestHeader): Option[String] = request.session.get("email")

  def onUnauthorized(request: RequestHeader): SimpleResult = {
    println(mail(request))
    Results.Redirect(routes.Application.login())
  }

  def withAuth(f: => String => Request[AnyContent] => SimpleResult) = {
    Security.Authenticated(mail, onUnauthorized) { user =>
      Action(request =>
        f(user)(request)
      )
    }
  }
}

object Application extends Controller with Auth {

  def loginAuth = Action { implicit request =>
    try{
      val address = request.body.asFormUrlEncoded.get("userName")(0)
      val pw = request.body.asFormUrlEncoded.get("pw")(0)
      // ここでマッチングの検証
      if( EntrantAdministrator.isUser(Option(address),pw)){
        Redirect(routes.Application.index()).withSession("email" -> address)
      }
      else{
        Redirect(routes.Application.login())
      }
    }catch{
      case _:Throwable => {
        println("なんかエラー出た。")
        Redirect(routes.Application.login())
      }
    }
  }

  def getText(mid:String) = withAuth{ mail => implicit request =>

    //TODO:リファクタ
    // Meetingからとってくればいいかな。
    val meetingData =  Meeting.getMeetingData(mid)
    if(!meetingData.isDefined){
      Redirect(routes.Application.recruit(mid))
    }
    else {
      val csvHeader: String = meetingData.get.meetingName + "\r\n"
      val csvColumns = "参加者名,備考" + meetingData.get.optionColumn.getOrElse(Seq()).mkString(",",",","") + "\r\n"

      val list = if (EntrantAdministrator.isAdministrator(Option(mail))) {
        EntrantUser.getEntrantListForAdminUser(mid)
      } else {
        EntrantUser.getEntrantList(mid, mail)
      }
      var listEntrantCSV: List[String] = List()
      for (entrant <- list) {
        var line = entrant.entrantName + "," + entrant.remarks.getOrElse("")
        if (entrant.optionData.isDefined) {
          for (option <- entrant.optionData.get) {
            line = line + "," + option.value.getOrElse("")
          }
        }
        listEntrantCSV = line :: listEntrantCSV
      }
      val csv = csvHeader + csvColumns + listEntrantCSV.mkString("\r\n")
      Ok(csv).withHeaders(
         CONTENT_TYPE -> "text/csv;"
        ,"Content-Disposition" -> s"attachment; filename=${meetingData.get._id.get}.csv"
      )
    }
  }


  def logout = Action { implicit request =>
    Redirect(routes.Application.login()).withNewSession
  }

  def index = withAuth{ mail => implicit request =>
    Ok(views.html.index(request.session.get("email"),EventRepository.get(),Meeting.getMeetingUserData))
  }

  def login = Action {
    Ok(views.html.login())
  }

  def recruit(mid: String) =  withAuth{ mail => implicit request =>
    try {
      val email = request.session.get("email")
      val data = Meeting.getMeetingData(mid)
      if(EntrantAdministrator.isAdministrator(email)){
        val list = EntrantUser.getEntrantListForAdminUser(mid)
        Ok(views.html.recruit(data.get, email, list))
      }
      else {
        val errString = Meeting.canEntrant(data)
        if (errString != MeetingErrorState.success) {
          Redirect(routes.Application.error(errString))
        }
        else {
          val list = EntrantUser.getEntrantList(mid, email.get)
          Ok(views.html.recruit(data.get, email, list))
        }
      }
   }
    catch{
      case _:Throwable =>
        Redirect(routes.Application.error("エラーが発生しました。"))
    }
  }

  def entrant(mid:String) = withAuth{ mail => implicit request =>
    val users = request.body.asFormUrlEncoded.get(mid)(0)
    val remarks = request.body.asFormUrlEncoded.get("remark_" + mid )(0)
    // OPTION Data追加
    var list:List[optionData] = List()
    val meetingData = Meeting.getMeetingData(mid).get
    if(meetingData.optionColumn.isDefined){
      for(option <- meetingData.optionColumn.get){
        val optText = request.body.asFormUrlEncoded.get(option+ "." + mid )(0)
        if(!optText.isEmpty) {
          list = optionData(option, Option(optText)) :: list
        }
      }
    }

    users.split(",").foreach(
      x =>
        println(EntrantUser.entrant(x , mid, Option(remarks),request.session.get("email").get,Option(list.toSeq)))
    )
    Redirect(routes.Application.recruit(mid))
  }

  def cancelEntrant(eid:String, mid:String) =  withAuth{ mail => implicit  request =>
    EntrantUser.cancelEntrant(eid)
    Redirect(routes.Application.recruit(mid))
  }

  //汎用エラー画面
  def error(err: String) = Action{ request =>
    Ok(views.html.error(err))
  }

}

object Admin extends Controller with Auth {
  def adminIndex =  withAuth{ mail => implicit  request =>
    if (!EntrantAdministrator.isAdministrator(request.session.get("email"))) {
      Redirect(routes.Application.index)
    }
    else {
      Ok(views.html.Admin(EventRepository.get(), EntrantAdministrator.getMeetingAdminData))
    }
  }

  def adminDetail(mid:String) = Action{ implicit request =>
    Ok(views.html.AdminDetail(mid,Meeting.getMeetingData(mid)))
  }

  def createEvent(eventName: String) = Action {
    EntrantAdministrator.createEvent(eventName, true)
    Redirect(routes.Admin.adminIndex())
  }

  def createMeeting(eventName: String, meetingName: String) = Action {
    val _id = EventRepository.filter(x => x.eventName == eventName).apply(0)._id.getOrElse(None)
    EntrantAdministrator.createMeeting(_id.toString, meetingName, true, None, None)
    Redirect(routes.Admin.adminIndex())
  }

  def delMeeting(mid:String) = Action { implicit  request =>
    Meeting.removeMeeting(mid)
    Redirect(routes.Admin.adminIndex())
  }

  def delOpt(mid:String, name:String) = Action { implicit request =>
    Meeting.removeMeetingOption(mid,name)
    Redirect(routes.Admin.adminDetail(mid))
  }

  def addOpt(mid:String) = Action{ implicit  request =>
    val optCol = request.body.asFormUrlEncoded.get("optionColName")(0)
    Meeting.addOptionColumns(mid,optCol)
    Redirect(routes.Admin.adminDetail(mid))
  }

  def addSetsumei(mid:String) = Action{ implicit  request =>
    val setsumei = request.body.asFormUrlEncoded.get("setsumei_" + mid)(0)
    Meeting.addSetsumei(mid,setsumei)
    Redirect(routes.Admin.adminDetail(mid))
  }

  def addMeeting = Action { implicit request =>
    val eventName = request.body.asFormUrlEncoded.get("eventName")(0)
    val meetingName = request.body.asFormUrlEncoded.get("meetingName")(0)
    val recruitTo = request.body.asFormUrlEncoded.get("recruitTo")(0)
    val check: Boolean =
      try {
        request.body.asFormUrlEncoded.get("recruitOn")(0)
        true
      }
      catch {
        case _:Throwable => false
      }

    println(check)
    //var df:DateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.JAPAN);
    val _id = EventRepository.filter(x => x.eventName == eventName).apply(0)._id.getOrElse(None)
    if (recruitTo == "") {
      println(recruitTo)
      EntrantAdministrator.createMeeting(_id.toString, meetingName, check, None, None)
    }
    else {
      println(recruitTo)
      val df: DateFormat = new SimpleDateFormat("yyyy-MM-dd")
      df.setTimeZone(TimeZone.getTimeZone("JST"))
      EntrantAdministrator.createMeeting(_id.toString, meetingName, check, None, Option(df.parse(recruitTo)))
    }
    Redirect(routes.Admin.adminIndex())
  }

  def stopMeeting(mid:String) = Action { implicit request =>
    Meeting.stop(mid)
    Redirect(routes.Admin.adminIndex())
  }

  def startMeeting(mid:String) = Action { implicit request =>
    Meeting.start(mid)
    Redirect(routes.Admin.adminIndex())
  }
}


object Auth extends Controller {
  /*
  def authenticate = Action { implicit request =>

    val f: Future[String] = OpenID.redirectURL(
      "www.google.com/accounts/o8/id",
      //routes.Auth.openIDCallback.absoluteURL().replace("127.0.0.1:9000", "192.168.33.20"),
      routes.Auth.openIDCallback.absoluteURL(),//.replace("127.0.0.1:9000", "saxia.dip.jp"),
      //"http://saxia.dip.jp/auth_callback",
      Seq(
        "email" -> "http://axschema.org/contact/email",
        "firstname" -> "http://axschema.org/namePerson/first",
        "lastname" -> "http://axschema.org/namePerson/last"
      )
    )
    f onFailure { case _ => Redirect("http://www.google.co.jp/")}
    AsyncResult(f map (Redirect(_)))
  }

  def openIDCallback = Action { implicit request =>
    val f: Future[UserInfo] = OpenID.verifiedId
    f onFailure { case _ => Redirect("http://www.google.co.jp/")}
    AsyncResult(
      f map (info =>
        Redirect(routes.Application.index).withSession(
          "openid" -> info.id,
          "firstName" -> info.attributes("firstname"),
          "lastName" -> info.attributes("lastname"),
          "email" -> info.attributes("email")
        )
        )
    )
  }

  def logout = Action { implicit request =>
    Redirect(routes.Application.login).withNewSession
  }
  */
}