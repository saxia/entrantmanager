package infra

import com.mongodb.casbah.Imports._
/**
 * Created by saxia on 2014/09/29.
 */
trait MongoDBAccessorTrait {
  private val host = "127.0.0.1"
  private val port = 27017
  private val databaseName = "entrantDb"
  private val client = MongoClient(host, port)
  private val threadLocal = new ThreadLocal[MongoDB]

  private def db(dbName:String):MongoDB = client(dbName)

  def getInstance() = {
    getInstanceByDataBaseName(databaseName)
  }

  def getInstanceByDataBaseName(dbName:String) = {
    Option(threadLocal.get()).getOrElse({
      val dbInstance = db(dbName)
      threadLocal.set(dbInstance)
      dbInstance
    })
  }
}

object MongoDBAccessor extends MongoDBAccessorTrait
