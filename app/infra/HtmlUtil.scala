package infra

/**
 * Created by saxia on 2014/11/07.
 */
object HtmlUtil {
  def convertToHtml(src:Option[String]): Option[String] ={
    if(src == None){
      return None
    }
    else{
      Option(src.get.replaceAll("(\r\n|\r|\n)","<br>"))
    }
  }
}
