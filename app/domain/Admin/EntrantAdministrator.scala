package domain.Admin

import java.text.{SimpleDateFormat, DateFormat}
import java.util.{TimeZone, Date}

import domain.Repository._

/**
 * Created by saxia on 2014/09/28.
 */
object EntrantAdministrator {

  // Eventを作成する。例：C87
  def createEvent(eventName: String, isOpen:Boolean) = {
    // 同じやつがなければ、
    if( EventRepository.filter(x => x.eventName == eventName).length == 0) {
      EventRepository.insert(EventData(None, eventName, isOpen.toString))
    }
  }

  // Eventを削除する。またEventに紐つくデータすべて削除
  def dropEvent(eventName: String) = {
    // ひとまずいいや…。
  }

  // Meetingを作成する。
  def createMeeting(EventId: String, meetingName: String, recruitStart: Boolean,  recruitNum: Option[Int],recruitTo: Option[Date]) = {
    if( recruitTo.isDefined ) {
      val recruitToString = EntrantDateFormat.getDataDateFormat.format(recruitTo.get)
      MeetingRepository.insert(MeetingData(None, EventId, meetingName, recruitStart.toString, recruitNum, Option(recruitToString)))
    }else{
      MeetingRepository.insert(MeetingData(None, EventId, meetingName, recruitStart.toString, recruitNum, None))
    }
  }

  // アクセス可能ユーザーを作成する
  def createUser(name: String, email: String, canAdministration: Boolean) = {

  }

  // 君はアドミニストレーター？
  def isAdministrator(email: Option[String]): Boolean = {
    if (email.getOrElse("") == "saxia.re@gmail.com") {
      return true
    }
    false
  }

  def isUser(email: Option[String], password:String):Boolean = {
    val list:List[(String, String)] = List(
      "test@example.com" -> "test",
      "saxia.re@gmail.com" -> "nekoneko"
    )
    list.contains(email.get -> password)
  }

  // Admin用ミーティング一覧
  def getMeetingAdminData:List[MeetingAdminData] = {
    var list:List[MeetingAdminData] = List()
    MeetingRepository.get.foreach(
      x => {
        val event = EventRepository.filter(y => y._id.getOrElse("") == x.event_id).apply(0)
        val default_date = EntrantDateFormat.getDataDateFormat.format(new Date())
        val recruitTo = x.recruitTo.getOrElse(default_date)
        list = MeetingAdminData(x._id.getOrElse("データ破損") , event.eventName,x.meetingName,
          Option(if(recruitTo != default_date){
            EntrantDateFormat.convert2view(recruitTo)
          }else{
            ""
          }), x.recruitStart.toBoolean, x.optionColumn) :: list
      }
    )
    list
  }

}
