package domain.Admin

/**
 * Created by saxia on 2014/10/03.
 */
case class MeetingAdminData
(
 _id:String
 , eventName:String
 , meetingName:String
 , viewDate:Option[String]
 , recruitStart:Boolean
 , optionColumn:Option[Seq[String]]
  )
