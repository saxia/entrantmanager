package domain

import java.text.{SimpleDateFormat, DateFormat}
import java.util.{TimeZone, Date}

import domain.Admin.MeetingAdminData
import domain.Repository._


object MeetingErrorState {
  val overNum = "人数を超過しています。"
  val endRecruit = "募集期間が終了しています。"
  val hasNoData = "会議データがありません。"
  val success = "可能"
}

object Meeting {

  //IDのミーティングを取得する。
  def getMeetingData(mid: String): Option[MeetingData] = {
    try {
      return Option(MeetingRepository.filter(x => x._id.getOrElse("") == mid).apply(0))
    }
    catch {
      case _: Throwable => None
    }
  }

  /**
   * 今ミーティングに参加可能か？（時間的に）
   */
  def canEntrantNow(meetingRow: MeetingData): Boolean = {
    if (meetingRow.recruitTo.isEmpty) {
      return true
    }
    val df: DateFormat = new SimpleDateFormat("EEE MMM dd hh:mm:ss zzz yyyy")
    df.setTimeZone(TimeZone.getTimeZone("JST"))
    val recruitTo = df.parse(meetingRow.recruitTo.get)
    val now = new Date()
    (now.getTime < recruitTo.getTime)
  }

  /**
   * 予定人数超えてないか？
   */
  def isOverNumOfMember(meetingRow: MeetingData): Boolean = {
    if (meetingRow.recruitNum.isEmpty) {
      return false
    }
    EntrantRepository.filter(x => x.meetingId == meetingRow._id.get).length > meetingRow.recruitNum.get
  }

  // ミーティングに登録可能か？
  def canEntrant(meetingData: Option[MeetingData]): String = {
    if (meetingData.isEmpty) {
      return MeetingErrorState.hasNoData
    }
    val meeting = meetingData.get
    if (Meeting.isOverNumOfMember(meeting)) {
      return MeetingErrorState.overNum
    }
    if (!canEntrantNow(meeting) || !meeting.recruitStart.toBoolean) {
      return MeetingErrorState.endRecruit
    }
    MeetingErrorState.success
  }

  // ユーザーにリスト見せる
  // Todo:リファクタ　アドミンのところとかぶってる
  def getMeetingUserData: List[MeetingAdminData] = {
    var list: List[MeetingAdminData] = List()
    MeetingRepository.filter(
      m => canEntrant(Option(m)) == MeetingErrorState.success
    ).foreach(
        x => {
          val event = EventRepository.filter(y => y._id.getOrElse("") == x.event_id).apply(0)
          val default_date = EntrantDateFormat.getDataDateFormat.format(new Date())
          val recruitTo = x.recruitTo.getOrElse(default_date)
          list = MeetingAdminData(x._id.getOrElse("データ破損"), event.eventName, x.meetingName,
            Option(if (recruitTo != default_date) {
              EntrantDateFormat.convert2view(recruitTo)
            } else {
              ""
            }), x.recruitStart.toBoolean, x.optionColumn) :: list
        }
      )
    list
  }

  def addOptionColumns(meetingId: String, optName: String) = {
    if (MeetingRepository.filter(row => row._id == Some(meetingId)).length == 0) {
      throw new IllegalArgumentException("meetingId Not Found")
    }
    MeetingRepository.addOptionColumn(meetingId, optName)
  }

  def addSetsumei(meetingId: String, setsumei: String) = {
    if (MeetingRepository.filter(row => row._id == Some(meetingId)).length == 0) {
      throw new IllegalArgumentException("meetingId Not Found")
    }
    MeetingRepository.addSetsumei(meetingId, setsumei)
  }


  // Meetingを削除
  def removeMeeting(meetingId: String) = {
    EntrantRepository.removeFromMeetingId(meetingId)
    MeetingRepository.removeFromId(meetingId)
  }

  def stop(meetingId: String) {
    MeetingRepository.updateRecruitStart(meetingId, false)
  }

  def start(meetingId: String) {
    MeetingRepository.updateRecruitStart(meetingId, true)
  }

  def removeMeetingOption(meetingId: String, optionCol: String) = {
    MeetingRepository.deleteOptionColumn(meetingId, optionCol)
    EntrantRepository.removeFromOptionName(meetingId,optionCol)
  }
}
