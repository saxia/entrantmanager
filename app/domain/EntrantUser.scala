package domain

import java.text.{SimpleDateFormat, DateFormat}

import domain.Repository._
import java.util.{TimeZone, Locale, Date}

object EntrantUser {

  // 登録する
  def entrant
  (
    entrantName: String
    , meetingId: String
    , remarks: Option[String]
    , entrantFrom: String
    , optionData: Option[Seq[optionData]] = None
    ): String = {
    val list = MeetingRepository.filter(x => x._id.getOrElse("") == meetingId)
    if (list.length <= 0) {
      return "meetingDataがありません。"
    }
    val meetingRow = list.apply(0)
    val stat = Meeting.canEntrant(Option(list.apply(0)))
    if (stat != MeetingErrorState.success) {
      stat
    }
    else {
      EntrantRepository.insert(EntrantData(None, meetingId, entrantName, remarks, entrantFrom, optionData))
      "登録完了"
    }
  }

  def getEntrantList(meetingId: String, email: String) = {
    getEntrantListForAdminUser(meetingId).filter(f => f.entrantFrom == email)
  }

  def getEntrantListForAdminUser(meetingId: String) = {
    EntrantRepository.filter(f => f.meetingId == meetingId)
  }

  // 登録を解除する。
  def cancelEntrant(entrantId: String) = {
    EntrantRepository.removeFromId(entrantId)
  }
}
