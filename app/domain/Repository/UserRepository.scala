package domain.Repository

import com.mongodb.DBObject
import com.mongodb.casbah.commons.MongoDBObject
import com.mongodb.util.JSON
import infra.MongoDBAccessor
import org.bson.types.ObjectId
import play.api.libs.functional.syntax._
import play.api.libs.json._
import scala.collection.JavaConversions._

/**
 * Created by saxia on 2014/10/05.
 */
case class UserData
(
_id :Option[String]
, mail:String
, password:String
, isAdmin:String
  )

object UserData{
  implicit val eventDataRead: Reads[UserData] = (
    (__ \ "_id" \ "$oid").readNullable[String] and
      (__ \ "eventName").read[String] and
      (__ \ "password").read[String] and
      (__ \ "isAdmin").read[String]
    )(UserData.apply _)

  implicit val entrantDataWrite: Writes[UserData] = (
    (__ \ "_id" \ "$oid").writeNullable[String] and
      (__ \ "eventName").write[String] and
      (__ \ "password").write[String] and
      (__ \ "isAdmin").write[String]
    )(unlift(UserData.unapply))
}

object UserRepository extends MongoAccessRepository[UserData]{
  val collectionName = "user"
  val collection = MongoDBAccessor.getInstance().getCollection(collectionName)


  override def insert(insertData: UserData): Unit = {
    collection.insert(JSON.parse(Json.toJson(insertData).toString).asInstanceOf[DBObject])
  }
  override def get: List[UserData] = {
    var listData: List[UserData] = List()
    collection.find().iterator().foreach(
      x =>  listData =  Json.parse(x.toString).as[UserData] :: listData
    )
    listData
  }

  override def removeFromId(id: String): Unit =  {
    collection.findAndRemove(MongoDBObject("_id" -> id))
  }
}
