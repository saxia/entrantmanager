package domain.Repository

import java.util.Date

import com.mongodb.DBObject
import com.mongodb.casbah.commons.MongoDBObject
import com.mongodb.util.JSON
import infra.MongoDBAccessor
import play.api.libs.json._
import play.api.libs.functional.syntax._
import scala.collection.JavaConversions._

/**
 * Created by saxia on 2014/09/29.
 */

case class EventData
(
  _id: Option[String]
 , eventName: String
 , isOpen: String
)

object EventData{
  implicit val eventDataRead: Reads[EventData] = (
    (__ \ "_id" \ "$oid").readNullable[String] and
      (__ \ "eventName").read[String] and
      (__ \ "isOpen").read[String]
    )(EventData.apply _)

  implicit val entrantDataWrite: Writes[EventData] = (
    (__ \ "_id").writeNullable[String] and
      (__ \ "eventName").write[String] and
      (__ \ "isOpen").write[String]
    )(unlift(EventData.unapply))
}


object EventRepository extends MongoAccessRepository[EventData] {
  val collectionName = "event"
  val collection = MongoDBAccessor.getInstance().getCollection(collectionName)

  def get(): List[EventData] = {

    var listData: List[EventData] = List()
    collection.find().iterator().foreach(
      x => listData = Json.parse(x.toString).as[EventData] :: listData
    )
    listData
  }

  def insert(insertData: EventData): Unit = {
    //println(Json.toJson(insertData).toString)
    collection.insert(JSON.parse(Json.toJson(insertData).toString).asInstanceOf[DBObject])
  }

  def removeFromId(id:String) = {
    collection.findAndRemove(MongoDBObject("_id" -> id))
  }
}