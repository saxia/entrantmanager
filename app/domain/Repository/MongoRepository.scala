package domain.Repository

/**
 * Created by saxia on 2014/09/29.
 */
// <: Extend2Seq (何かを継承してるTの場合）
trait MongoAccessRepository[T]{
  def filter(f:((T) => Boolean)):List[T] = {
    get.filter(f)
  }
  def removeFromId(id:String)

  def insert(insertData:T):Unit
  def get:List[T]
}

trait Extend2Seq{
  def toSeq:Seq[(String,String)]
}