package domain.Repository

import com.mongodb.DBObject
import com.mongodb.casbah.commons.MongoDBObject
import com.mongodb.util.JSON
import infra.MongoDBAccessor
import org.bson.types.ObjectId
import scala.collection.JavaConversions._

import play.api.libs.json._
import play.api.libs.functional.syntax._

case class EntrantData
(_id: Option[String]
 , meetingId: String
 , entrantName: String
 , remarks: Option[String] = None
 , entrantFrom: String
 , optionData: Option[Seq[optionData]] = None
  )

object EntrantData {
  implicit val entrantDataRead: Reads[EntrantData] = (
    (__ \ "_id" \ "$oid").readNullable[String] and
      (__ \ "meetingId").read[String] and
      (__ \ "entrantName").read[String] and
      (__ \ "remarks").readNullable[String] and
      (__ \ "entrantFrom").read[String] and
      (__ \ "optionData").readNullable[Seq[optionData]]
    )(EntrantData.apply _)

  implicit val entrantDataWrite: Writes[EntrantData] = (
    (__ \ "_id").writeNullable[String] and
      (__ \ "meetingId").write[String] and
      (__ \ "entrantName").write[String] and
      (__ \ "remarks").writeNullable[String] and
      (__ \ "entrantFrom").write[String] and
      (__ \ "optionData").writeNullable[Seq[optionData]]
    )(unlift(EntrantData.unapply))
}

case class optionData
(
  columnsName: String
  , value: Option[String]
  )

object optionData {
  implicit val optionDataFormat: Format[optionData] = (
    (__ \ "columnsName").format[String] ~
      (__ \ "value").formatNullable[String]
    )(optionData.apply, unlift(optionData.unapply))
}

/**
 * 登録者リポジトリ
 */
object EntrantRepository extends MongoAccessRepository[EntrantData] {
  val collectionName = "entrant"
  val collection = MongoDBAccessor.getInstance().getCollection(collectionName)

  def get: List[EntrantData] = {
    var list: List[EntrantData] = List()
    collection.find().iterator().foreach(
      x => list = Json.parse(x.toString).as[EntrantData] :: list
    )
    list
  }

  def insert(insertData: EntrantData): Unit = {
    //println(Json.toJson(insertData).toString)
    collection.insert(JSON.parse(Json.toJson(insertData).toString).asInstanceOf[DBObject])
  }

  def removeFromId(id: String) = {
    val objectId: ObjectId = new ObjectId(id)
    val result = collection.findAndRemove(MongoDBObject("_id" -> objectId))
    println("result: " + result)
  }

  def removeFromMeetingId(meetingId: String): Unit = {
    val result = collection.remove(MongoDBObject("meetingId" -> meetingId))
    println("result: " + result)
  }

  def removeFromOptionName(meetingId: String, optionColName: String): Unit = {

    val finded = filter(q => (q.meetingId == meetingId &&
      q.optionData.getOrElse(Seq()).filter(x => x.columnsName == optionColName).length > 0))

    println(finded)
    for (entrantData <- finded) {
      val data = entrantData.optionData.get
      val removedData = data.dropWhile(x => x.columnsName == optionColName)
      val result = collection.update(MongoDBObject("_id" -> new ObjectId(entrantData._id.get)),
        MongoDBObject(
          List(
            "meetingId" -> Some(entrantData.meetingId)
            , "entrantName" -> Some(entrantData.entrantName)
            , "remarks" -> entrantData.remarks
            , "entrantFrom" -> Some(entrantData.entrantFrom)
            , "optionData" -> Some(removedData)
          ).filter(_._2.isDefined) map (x => (x._1, x._2.get))
        ))
      println(result)
    }
  }
}
