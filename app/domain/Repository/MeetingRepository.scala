package domain.Repository

import java.text.{SimpleDateFormat, DateFormat}
import java.util.{TimeZone, Locale, Date}

import com.mongodb.DBObject
import com.mongodb.casbah.commons.MongoDBObject
import com.mongodb.util.JSON
import infra.MongoDBAccessor
import org.bson.types.ObjectId
import scala.collection.JavaConversions._

import play.api.libs.json._
import play.api.libs.functional.syntax._

object EntrantDateFormat {
  def getDataDateFormat: DateFormat = {
    val df: DateFormat = new SimpleDateFormat("EEE MMM dd hh:mm:ss zzz yyyy")
    df.setTimeZone(TimeZone.getTimeZone("JST"))
    df
  }

  def getViewDataFormat: DateFormat = {
    val df: DateFormat = new SimpleDateFormat("zzz yyyy/MM/dd EEE")
    df.setTimeZone(TimeZone.getTimeZone("JST"))
    df
  }

  def convert2view(dateString: String): String = {
    getViewDataFormat.format(getDataDateFormat.parse(dateString))
  }
}


case class MeetingData
(_id: Option[String]
 , event_id: String
 , meetingName: String
 , recruitStart: String
 , recruitNum: Option[Int]
 , recruitTo: Option[String]
 , optionColumn: Option[Seq[String]] = None
 , setsumei:Option[String] = None
  )

object MeetingData {
  implicit val meetingDataRead: Reads[MeetingData] = (
    (__ \ "_id" \ "$oid").readNullable[String] and
      (__ \ "eventId").read[String] and
      (__ \ "meetingName").read[String] and
      (__ \ "recruitStart").read[String] and
      (__ \ "recruitNum").readNullable[Int] and
      (__ \ "recruitTo").readNullable[String] and
      (__ \ "optionColumn").readNullable[Seq[String]] and
      (__ \ "setsumei").readNullable[String]
    )(MeetingData.apply _)

  implicit val entrantDataWrite: Writes[MeetingData] = (
    (__ \ "_id").writeNullable[String] and
      (__ \ "eventId").write[String] and
      (__ \ "meetingName").write[String] and
      (__ \ "recruitStart").write[String] and
      (__ \ "recruitNum").writeNullable[Int] and
      (__ \ "recruitTo").writeNullable[String] and
      (__ \ "optionColumn").writeNullable[Seq[String]] and
      (__ \ "setsumei").writeNullable[String]
    )(unlift(MeetingData.unapply))
}

object MeetingRepository extends MongoAccessRepository[MeetingData] {
  val collectionName = "meeting"
  val collection = MongoDBAccessor.getInstance().getCollection(collectionName)

  def get(): List[MeetingData] = {
    var listData: List[MeetingData] = List()
    collection.find().iterator().foreach(
      x => listData = Json.parse(x.toString).as[MeetingData] :: listData
    )
    listData
  }

  def insert(insertData: MeetingData): Unit = {
    collection.insert(JSON.parse(Json.toJson(insertData).toString).asInstanceOf[DBObject])
  }

  def updateRecruitStart(mid: String, recruitStart: Boolean) = {
    var data = filter(d => d._id == Option(mid)).apply(0)
    collection.update(MongoDBObject("_id" -> new ObjectId(mid)), MongoDBObject(
      List(
        "eventId" -> Some(data.event_id),
        "meetingName" -> Some(data.meetingName),
        "recruitStart" -> Some(recruitStart.toString),
        "recruitNum" -> data.recruitNum,
        "recruitTo" -> data.recruitTo,
        "optionColumn" -> data.optionColumn,
        "setsumei" -> data.setsumei
      ).filter(_._2.isDefined) map (x => (x._1, x._2.get))
    ))
  }

  private def createColName(list: List[String], colName: String, num: Int = 0): String = {
    if (list.filter(p => p == colName).length == 0) {
      return colName
    }
    val newName = colName.split("_")(0) + "_" + num.toString
    createColName(list, newName, (num + 1))
  }

  def addOptionColumn(mid: String, colName: String) = {
    val data = filter(d => d._id == Option(mid)).apply(0)

    val add =
      if (!data.optionColumn.isDefined) {
        Option(Seq(colName))
      } else {
        var base = data.optionColumn.get.toList
        base = createColName(base, colName) :: base
        Option(base.toSeq)
      }
    collection.update(MongoDBObject("_id" -> new ObjectId(mid)), MongoDBObject(
      List(
        "eventId" -> Some(data.event_id),
        "meetingName" -> Some(data.meetingName),
        "recruitStart" -> Some(data.recruitStart),
        "recruitNum" -> data.recruitNum,
        "recruitTo" -> data.recruitTo,
        "optionColumn" -> add,
        "setsumei" -> data.setsumei
      ).filter(_._2.isDefined) map (x => (x._1, x._2.get))
    ))
  }

  def addSetsumei(mid: String, setsumei: String) = {
    val data = filter(d => d._id == Option(mid)).apply(0)

    collection.update(MongoDBObject("_id" -> new ObjectId(mid)), MongoDBObject(
      List(
        "eventId" -> Some(data.event_id),
        "meetingName" -> Some(data.meetingName),
        "recruitStart" -> Some(data.recruitStart),
        "recruitNum" -> data.recruitNum,
        "recruitTo" -> data.recruitTo,
        "optionColumn" -> data.optionColumn,
        "setsumei" -> Option(setsumei)
      ).filter(_._2.isDefined) map (x => (x._1, x._2.get))
    ))
  }

  def deleteOptionColumn(mid: String, colName: String) = {
    val data = filter(d => d._id == Option(mid)).apply(0)

    if (!data.optionColumn.isDefined) {
      throw new Exception("データが存在しません！")
    }
    val options = data.optionColumn.get.toList
    val del = options.dropWhile(x => x == colName)

    collection.update(MongoDBObject("_id" -> new ObjectId(mid)),
      MongoDBObject(
        List(
          "eventId" -> Some(data.event_id),
          "meetingName" -> Some(data.meetingName),
          "recruitStart" -> Some(data.recruitStart),
          "recruitNum" -> data.recruitNum,
          "recruitTo" -> data.recruitTo,
          "optionColumn" -> Option(del),
          "setsumei" -> data.setsumei
    ).filter(_._2.isDefined).map(x => (x._1, x._2.get))
      ))
  }

  def removeFromId(id: String) = {
    val objectId: ObjectId = new ObjectId(id)
    val result = collection.findAndRemove(MongoDBObject("_id" -> objectId))
    println("result: " + result)
  }

  def removeFromEventId(eventId: String) = {
    val result = collection.remove(MongoDBObject("eventId" -> eventId))
    println("result: " + result)
  }
}